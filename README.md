Git
===

Installation
------------

Instructions for installing git on multiple OSes are
[here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). 

Plan
----

* Email installation instructions and request that ppl bring laptops
* Start by just having everyone clone the directory, add their own file, commit,
  push, pull, and inspect
* Then follow this tutorial for the conceptual guide: http://rogerdudler.github.io/git-guide/
